
## Description

This module adds an IBAN Field using the Drupal Field API.
The module comes shipped with a second module (Webform IBAN) which can be used
to extend the functionality to the Webform module found at:
[Webform module](https://drupal.org/project/webform)

> IBAN numbers are validated by [Symfony IBAN validator](https://symfony.com/doc/3.4/reference/constraints/Iban.html)

## Requirements

* Drupal 8, 9 or 10
* Webform module (https://drupal.org/project/webform)
(Webform module only required when also enabling webform_iban)


## Installation

1. Copy the entire iban_field directory the Drupal sites/modules directory or
   ```composer require drupal/iban_field```
2. Login as an administrator. Enable the module via "Administer" -> "Modules".
3. Add a textfield to an entity and select the IBAN field formatter in the form
   display settings, or when using the Webform IBAN submodule, add the IBAN
   webform field on a webform.

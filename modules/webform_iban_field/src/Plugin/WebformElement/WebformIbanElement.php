<?php

namespace Drupal\webform_iban_field\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'webform_iban_element' element.
 *
 * @WebformElement(
 *   id = "webform_iban_element",
 *   label = @Translation("IBAN field"),
 *   description = @Translation("Provides a webform iban field."),
 *   category = @Translation("Basic elements"),
 * )
 *
 * @see \Drupal\webform_iban_field\Element\WebformIbanElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformIbanElement extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties(): array {
    return parent::getDefaultProperties() + [
      'multiple' => '',
      'size' => '',
      'minlength' => '',
      'maxlength' => '',
      'placeholder' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    return $form;
  }

}

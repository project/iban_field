<?php

namespace Drupal\webform_iban_field\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElementBase;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Validation;

/**
 * Provides a 'webform_iban_element'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have a corresponding FormElement.
 *
 * Below is the definition for a custom 'webform_iban_element' which just
 * renders a simple text field.
 *
 * @FormElement("webform_iban_element")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_iban_field\Element\WebformIbanElement
 */
class WebformIbanElement extends FormElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#process' => [
        [$class, 'processWebformElementIban'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformIbanElement'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformIbanElement'],
      ],
      '#theme' => 'input__webform_iban_element',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'webform_iban_element' element.
   */
  public static function processWebformElementIban(&$element, FormStateInterface $form_state, &$complete_form) {
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_iban_element'.
   */
  public static function validateWebformIbanElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    $validator = Validation::createValidator();
    $violations = $validator->validate($value, [new Iban()]);

    if (count($violations) !== 0) {
      // There is an IBAN violation. Report the error.
      $form_state->setError($element, t('This is not a valid International Bank Account Number (IBAN).'));
      return;
    }

    // Ensure IBAN is uppercase.
    $value = mb_strtoupper($value);
    $form_state->setValueForElement($element, $value);
  }

  /**
   * Prepares a #type 'text' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformIbanElement(array $element): array {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, [
      'id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
    ]);
    static::setAttributes($element, ['form-text', 'webform-iban-element']);
    return $element;
  }

}
